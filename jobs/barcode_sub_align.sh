#! /bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=8:00:00
#$ -P gentech-rnd.p
#$ -l exclusive.c
#$ -w e
#$ -j y
#$ -R y

##########################################################################################
# Aligns barcoded reads to reference and calls variants
##########################################################################################
#--- Run options
# Current directory
STARTDIR=$PWD
# Directory where runs are located
rundir="/global/dna/projectdirs/RD/synbio/Runs"
# PacBio sample name, i.e. HAR33306
[[ -z ${samp} ]] && echo 'No sample!' && exit 1
# Barcode number
[[ -z ${bcnum} ]] && echo 'No barcode!' && exit 1
# Path to reference sequences
[[ -z ${ref} ]] && echo 'No reference!' && exit 1
[[ ! -e ${ref} ]] && echo "Could not find reference: $ref" && exit 1
# If no pool, set to $samp
[[ -z ${pool} ]] && pool=${samp}

# Temporary destination
[[ -z ${dest} ]] && dest=`mktemp -d`
# Final destination (on DnA)
dnadest="/global/dna/projectdirs/RD/synbio/Analysis/${aname}"

#--- Input files
# Barcoded FASTQ reads
[[ -z ${subreads} ]] && subreads=${rundir}/${samp}/sub/bins/B$(printf "%03d" $bcnum).fastq
[[ ! -e ${subreads} ]] && echo "Could not find SUB reads: $subreads" && exit 1

basFofn=${rundir}/${samp}/input.fofn
bcFofn=${rundir}/${samp}/barcode.fofn

#--- Read filtering
# Trim barcodes off both sides. In most cases, reads have not been trimmed yet.
[[ -z ${bctrim} ]] && bctrim=0

#--- Output files
# trim_fastq=trimmed.ccs.fastq
# trim_fasta=trimmed.ccs.fasta
alnreads=sub/aligned_reads
gfffile=sub/snps.quiver.gff
confile=sub/consensus.quiver.fasta
# covfile=ccs/covdepth
# callbed=ccs/callable.bed
# vcffile=ccs/snps.gatk.vcf
# sumfile=ccs/call_summary.txt

#--- Print options
echo "Analysis name:      ${aname}"
echo "Sample ID:          ${samp}"
echo "Pool:               ${pool}"
echo "Barcode number:     ${bcnum}"
echo "Reference:          ${ref}"

echo "Temporary dir:      ${dest}"
echo "Final dir:          ${dnadest}"
echo "Trim reads:         ${bctrim}"


##########################################################################################
# Initialize run directory
##########################################################################################
umask 0002
export _JAVA_OPTIONS="-Xmx12G"

#--- Load the SBA module
module use /global/dna/projectdirs/RD/synbio/Modules
module load jgisb_analysis

#--- Load smrtanalysis
module load smrtanalysis/2.2.0

#--- Make output directory
mkdir -p ${dest} && cd ${dest}
mkdir -p sub

##########################################################################################
# Trim reads to remove barcodes
##########################################################################################
# SBA_trimmer --trim ${bctrim} < ${ccsreads} > ${trim_fastq}
SBA_fastq_to_fasta < ${subreads} > reads.fasta

##########################################################################################
# Align CCS reads using pbalign.py
##########################################################################################
#--- Align reads using blasr without using pulse information
pbalign.py \
  --nproc 8 \
  --minAccuracy 0.75 \
  --minLength 50 \
  --maxHits 1 \
  --hitPolicy randombest \
  --forQuiver \
  ${basFofn} \
  ${ref} \
  ${alnreads}.cmp.h5

pbbarcode labelAlignments \
  --minAvgBarcodeScore 23 \
  ${bcFofn} \
  ${alnreads}.cmp.h5

bcfmt=B$(printf "%03d" $bcnum)--B$(printf "%03d" $bcnum)

cmph5tools.py select \
  --where "Barcode == '$bcfmt'" \
  --groupBy Barcode \
  ${alnreads}.cmp.h5

cmph5tools.py stats --what "Count(Barcode)" \
                    --groupBy Barcode \
                    out.cmp.h5

cmph5tools.py sort out.cmp.h5

quiver -j8 out.cmp.h5 \
       -r ${ref} \
       -o ${gfffile} -o ${confile}




pbsamtools.py --bam --outfile ${alnreads}.sam ${alnreads}.cmp.h5


#--- Add quality scores from FASTQ file to SAM file
# Quality scores greater than 41 (J) need to be replaced by 41 (for GATK compatability)
# TLEN must be set to 0 for single-end reads
SBA_add_qual_to_sam ${trim_fastq} ${alnreads}.orig.sam | \
  perl -n -e '@c=split(/\t/); $c[10]=~s/[K-~]/J/g unless /^@/; print join("\t",@c)' | \
  perl -n -e '@c=split(/\t/); $c[8]="0" unless /^@/; print join("\t",@c)' > ${alnreads}.qual.sam

#--- Load picard
module load picard/1.92

#--- Reorder BAM file so it matches reference file
picard ReorderSam I=${alnreads}.qual.sam R=${ref} O=${alnreads}.unsorted.bam

#--- Sort and index BAM
picard SortSam I=${alnreads}.unsorted.bam O=${alnreads}.bam SORT_ORDER=coordinate
picard BuildBamIndex I=${alnreads}.bam O=${alnreads}.bam.bai

#--- Remove intermediate files
rm -f ${alnreads}.orig.* ${alnreads}.qual.* ${alnreads}.unsorted.*

##########################################################################################
# Run GATK
##########################################################################################
module load GATK/2.7-2

# FindCoveredIntervals
# Output intervals that are "uncovered" with depth < 5
GenomeAnalysisTK -T FindCoveredIntervals -R $ref -I ${alnreads}.bam -cov 5 -u -o ccs/uncovered.txt

# Callable loci
GenomeAnalysisTK -T CallableLoci -R $ref -I ${alnreads}.bam -summary ccs/callable.summary -o ccs/callable.bed

# Generate coverage at every base
GenomeAnalysisTK -T DepthOfCoverage -R $ref -I ${alnreads}.bam -o ${covfile}

# Call variants using Unified Genotyper
GenomeAnalysisTK -T UnifiedGenotyper -R $ref -I ${alnreads}.bam -glm BOTH --max_deletion_fraction 0.55 -o ${vcffile} -nt 8

##########################################################################################
# Summarize
##########################################################################################
module load dnassemble/1.0

SBA_calls_gatk --reffile ${ref} --vcffile ${vcffile} --covfile ${covfile} > ${sumfile}

##########################################################################################
# Copy files to DnA filesystem
##########################################################################################
#--- Transfer references
ssh dtn04.nersc.gov "umask 0002; mkdir -p $dnadest/refs"
rsync -av $(echo $ref | sed 's|.fasta$||')* dtn04.nersc.gov:$dnadest/refs

bcname=B$(printf "%03d" $bcnum)
ccsdest="$dnadest/$pool/$bcname/ccs"
ssh dtn04.nersc.gov "umask 0002; mkdir -p $ccsdest"

#--- rsync files
shortref=$(basename ${ref%%.*})
rsync -av ${alnreads}.bam dtn04.nersc.gov:${ccsdest}/${shortref}.bam
rsync -av ${alnreads}.bam.bai dtn04.nersc.gov:${ccsdest}/${shortref}.bam.bai
rsync -av ${callbed} dtn04.nersc.gov:${ccsdest}/${shortref}.callable.bed
rsync -av ${vcffile} dtn04.nersc.gov:${ccsdest}/${shortref}.vcf
rsync -av ${vcffile}.idx dtn04.nersc.gov:${ccsdest}/${shortref}.vcf.idx
rsync -av ${sumfile} dtn04.nersc.gov:${ccsdest}/${shortref}.summary.txt

cd $STARTDIR && rm -rf $dest

