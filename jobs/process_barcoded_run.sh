#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=8:00:00
#$ -P gentech-rnd.p
#$ -l exclusive.c
#$ -w e
#$ -j y
#$ -R y

##########################################################################################
# Performs barcode binning for a PacBio run
##########################################################################################

#--- Run options
# PacBio sample name, i.e. HAR33306
[[ -z ${samp} ]] && echo "No sample!" && exit 1
# List of barcode numbers. This should be a colon-delimited list; had difficulty passing 
# a comma-delimited list through qsub -v
[[ -z ${bclist} ]] && echo "No barcodes!" && exit 1
# Temporary directory
[[ -z ${dest} ]] && dest=`mktemp -d`
# Final destination (on DnA)
dnadest="/global/dna/projectdirs/RD/synbio/Runs/$samp"

#--- CCS Options
# Number of passes to generate CCS
[[ -z ${min_passes} ]]  && min_passes=2
# Percent accuracy for CCS read
[[ -z ${min_accuracy} ]] && min_accuracy=90

#--- Read filtering
# Minimum read length
[[ -z ${min_length} ]] && min_length=50
[[ -z ${min_subread} ]] && min_subread=50
# Minimum barcode score. Score of 23 is recommended for 16bp barcodes.
[[ -z ${min_bcscore} ]] && min_bcscore=23
# Trim barcodes off both sides. In most cases, leave reads untrimmed for now.
[[ -z ${bctrim} ]] && bctrim=0

#--- Print options
echo "Sample ID:          ${samp}"
echo "Barcode list:       ${bclist}"
echo "Temporary dir:      ${dest}"
echo "Final dir:          ${dnadest}"
echo "Min CCS passes:     ${min_passes}"
echo "Min CCS accuracy:   ${min_accuracy}"
echo "Min read length:    ${min_length}"
echo "Min subread length: ${min_subread}"
echo "Min barcode score:  ${min_bcscore}"
echo "Trim reads:         ${bctrim}"

##########################################################################################
# Initialize run directory
##########################################################################################
umask 0002
export _JAVA_OPTIONS="-Xmx12G"

#--- Load jgibio
module load jgibio

#--- Load the SBA module
module use /global/dna/projectdirs/RD/synbio/Modules
module load jgisb_analysis

#--- Load smrtanalysis
module load smrtanalysis/2.2.0

#--- Move to temporary directory
mkdir -p $dest && cd $dest

#--- Setup input files
SBA_find_pb_data $samp > input.fofn
fofnToSmrtpipeInput.py input.fofn > input.xml

#--- Create barcode reference file
SBA_generate_barcode_fasta --delim : $bclist > barcodes.fasta

##########################################################################################
# Use smrtpipe:fetch to gather input files
##########################################################################################
echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><smrtpipeSettings><module id="P_Fetch"></module></smrtpipeSettings>' > fetch.xml
mkdir -p fetch
smrtpipe.py --params fetch.xml --output fetch xml:input.xml
rm fetch.xml

##########################################################################################
# Generate circular consensus
##########################################################################################
#--- Run ConsensusTools
mkdir -p ccs
ConsensusTools.sh CircularConsensus \
  --minFullPasses ${min_passes} \
  --minPredictedAccuracy ${min_accuracy} \
  --chemistry fetch/data/chemistry_mapping.xml \
  --numThreads 16 \
  --fofn input.fofn \
  -o ccs

#--- Create fofn for ccs.h5 files
for f in ccs/*.ccs.h5; do echo $f; done > ccs.fofn

##########################################################################################
# Label barcoded reads
##########################################################################################
#--- Run pbbarcode:labelZmws
mkdir -p bcH5
pbbarcode labelZmws \
    --scoreMode symmetric \
    --nProcs 16 \
    --outDir bcH5 \
    barcodes.fasta \
    input.fofn

##########################################################################################
# Create FASTQ files for CCS
##########################################################################################
#--- Emit FASTQ for barcoded CCS reads
mkdir -p ccs/tmp
pbbarcode emitFastqs \
  --outDir ccs/tmp \
  --trim ${bctrim} \
  --minMaxInsertLength ${min_length} \
  --minAvgBarcodeScore ${min_bcscore} \
  ccs.fofn \
  barcode.fofn

#--- Fix sequence names in FASTQ files
# "/ccs" and additional information is added to sequence description line
mkdir -p ccs/bins
for f in ccs/tmp/*.fastq; do bc=$(basename ${f%%-*}) && cat $f | sed 's|/ccs .*$||' > ccs/bins/$bc.fastq; done

#--- Clear temporary directory
rm -rf ccs/tmp

#--- Emit FASTQ for non-barcoded reads
SBA_nobarcodeCCS \
  --outFile ccs/bins/nobarcode.fastq \
  --minMaxInsertLength ${min_length} \
  --minAvgBarcodeScore ${min_bcscore} \
  ccs.fofn \
  barcode.fofn

##########################################################################################
# Create FASTQ files for subreads
##########################################################################################
#--- Emit FASTQ for barcoded subreads
mkdir -p sub/tmp
pbbarcode emitFastqs \
  --outDir sub/tmp \
  --trim ${bctrim} \
  --minMaxInsertLength ${min_length} \
  --minAvgBarcodeScore ${min_bcscore} \
  input.fofn \
  barcode.fofn

#--- Fix sequence names in FASTQ files
# Additional information is added to sequence description line after whitespace
mkdir -p sub/bins
for f in sub/tmp/*.fastq; do bc=$(basename ${f%%-*}) && cat $f | cut -d' ' -f1 > sub/bins/$bc.fastq; done

#--- Clear temporary directory
rm -rf sub/tmp

#--- Emit FASTA for non-barcoded subreads
SBA_nobarcodeSubreads \
  --outFile sub/bins/nobarcode.fastq \
  --minMaxInsertLength ${min_length} \
  --minAvgBarcodeScore ${min_bcscore} \
  --minSubreadLength ${min_subread} \
  input.fofn \
  barcode.fofn

##########################################################################################
# Copy files to DnA filesystem
##########################################################################################
#--- Create destination directory
ssh dtn04.nersc.gov "umask 0002; mkdir -p $dnadest"

#--- rsync files
rsync -av * dtn04.nersc.gov:$dnadest

#--- edit fofn files to contain absolute path
ssh dtn04.nersc.gov "umask 0002; cd $dnadest && mv ccs.fofn ccs.fofn.bak && cat ccs.fofn.bak | while read p; do [ -e \$PWD/\$p ] && echo \$PWD/\$p; done > ccs.fofn && rm ccs.fofn.bak"
ssh dtn04.nersc.gov "umask 0002; cd $dnadest && mv barcode.fofn barcode.fofn.bak && cat barcode.fofn.bak | while read p; do [ -e \$PWD/\$p ] && echo \$PWD/\$p; done > barcode.fofn && rm barcode.fofn.bak"

exit 0
